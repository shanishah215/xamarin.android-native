using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.ViewModels;
using Google.Places;
using NewsReader.ViewModels;
using System;

namespace NewsReader.Android.Views
{
    [Activity(Label = "Favourite List")]
    public class FirstView : MvxActivity
    {
        protected FirstViewModel MainViewModel
        {
            get { return ViewModel as FirstViewModel; }
        }
        Database db;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.first_view);

            //Create Database  
            db = new Database();
            db.createDatabase();

            LoadData();

            if (!PlacesApi.IsInitialized)
            {
                PlacesApi.Initialize(this, "AIzaSyCjnqR-lFOdhzDzfRMJaSQFpLOGywvZ6ZQ");
            }

            ImageView imageAdd = FindViewById<ImageView>(Resource.Id.add);
            imageAdd.Click += FabOnClick;


        }

        private void LoadData()
        {
            var listSource = db.selectTable();
            for (int i = 0; i < listSource.Count; i++)
            {
                MainViewModel.addData(listSource[i].address);
            }
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            /* Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                 .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();*/
            JavaList<Place.Field> fields = new JavaList<Place.Field>();

            fields.Add(Place.Field.Id);
            fields.Add(Place.Field.Name);
            fields.Add(Place.Field.LatLng);
            fields.Add(Place.Field.Address);

            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.Fullscreen, fields)
                .Build(this);
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (data != null)
            {
                try
                {
                    var place = Autocomplete.GetPlaceFromIntent(data);
                    AddressModel person = new AddressModel()
                    {
                        address = place.Name
                    };
                    db.insertIntoTable(person);
                    LoadData();
                    //MainViewModel.addData(place.Name);
                    Toast.MakeText(this, place.Name, ToastLength.Long).Show();

                }
                catch (Exception e)
                {
                    //MainViewModel.addData(e.Message);
                    Toast.MakeText(this, e.Message, ToastLength.Long).Show();
                }
            }
        }
    }
}